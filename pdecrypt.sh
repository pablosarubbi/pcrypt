--------------------------------------------------------------------------------------
--                                PROCESO DE DESCIFRADO                             --
--------------------------------------------------------------------------------------
#!/bin/bash
ARCHIVO_TAR=$1
ARCHIVO=${ARCHIVO_TAR/%.tar/}

# Extraigo las partes cifradas
tar -xvf $ARCHIVO.tar

# Descifro cada una de las partes
for i in $( ls part_*.gpg ); do
    /usr/bin/gpg --armor --recipient pablo@sarubbi.com.ar --decrypt --output $i.txt $i &
done

# Espero hasta que termine el proceso de descifrado
while [ `ps -a | grep gpg | wc -l` -gt 0 ]
do
    sleep 3
done

# Elimino los archivos temporales
rm part_*.gpg

# Uno cada una de las partes ya en texto claro para formar el archivo original
cat part_*.txt > $ARCHIVO

# Elimino los archivos temporales
rm part_*.txt
--------------------------------------------------------------------------------------
--                                PROCESO DE CIFRADO                                --
--------------------------------------------------------------------------------------
#!/bin/bash
ARCHIVO=$1
CPUS=`nproc`
TAMANO=`ls -l $ARCHIVO | cut -d " " -f 5`
((CADA_PARTE=TAMANO/CPUS))

# Divido el archivo original en tantas partes como CPUs tengo
split $ARCHIVO -b $CADA_PARTE part_

# Cifro cada una de las partes con gpg
for i in $( ls part_* ); do
    /usr/bin/gpg --armor --recipient pablo@sarubbi.com.ar --encrypt --output $i.gpg $i &
done

# Espero hasta que terminen las tareas de cifrado
while [ `ps -a | grep gpg | wc -l` -gt 0 ]
do
    sleep 3
done

# Empaqueto las partes cifradas en un archivo .TAR
tar -cvf $ARCHIVO.tar part_*.gpg

# Elimino los archivos temporales
rm part_*